(function() {
    'use strict';

    if (CKEDITOR.plugins.get('tooltips_fix')){
        return;
    }

    CKEDITOR.plugins.add('tooltips_fix', {
        init: function(editor) {
            editor.once('instanceReady', evt => {
                const editable = evt.editor.editable();
                // Remove CK Editor accessibility tooltips
                editable.removeAttribute('aria-multiline');
                editable.removeAttribute('aria-label');
                editable.removeAttribute('title');

                // Remove field description tooltip
                const parent = editable.getParent();
                parent.removeAttribute('title');
                parent.removeAttribute('data-original-title');

                // Remove custom tag accessibility tooltips
                jQuery('body').on('DOMSubtreeModified', '.ae-ui', function() {
                    jQuery('.ae-button.ez-btn-ae').each(function() {
                        const element = jQuery(this);
                        if (element.attr('data-tooltip')) {
                            element.removeAttr('title');
                        }
                    });
                });
            });
        },
    });
})();
